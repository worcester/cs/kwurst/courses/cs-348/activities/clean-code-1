# Clean Code 4 - Comments

Functions are the building blocks of programs. Creating easy to read functions makes it easier to understand and modify our programs.

> Don’t comment bad code - rewrite it.
>
> Brian W. Kernighan and P.J. Plaugher

> Every time you express yourself in code, you should pat yourself on the back. Every time you write a comment, you should grimace and feel the failure of your ability of expression.
>
> Uncle Bob

## Content Learning Objectives

By the end of this activity, participants will be able to...

## Process Skill Goals

_During the activity, students should make progress toward:_

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: Comments

* Comments Do Not Make Up for Bad Code
* Explain Yourself in Code
* Comments can lie

### Model 1 - Questions (5 min)

1. What should you do if you feel like you need to comment some code?
2. What is most often the easiest way to deal with feeling like you need to write a comment?
3. Why do comments lie?

## Model 2 - Good Comments

* Legal Comments - needed for licenses, etc.
* Informative Comments - provide basic information
* Explanation of Intent - explain why something was done
* Clarification - make something clear that you cannot recode
* Warning of Consequences - warn others about the consequences of some action
* TODO Comments - note something that needs to be done later
* Amplification - make sure that something that is important is obvious
* Javadocs in Public APIs

### Model 2 - Questions (8 min)

1. Explain why an “explanation of intent” comment might be needed. What may be hard to express in code?
2. Explain why a “warning of consequences” comment might be needed.
3. Explain why an “amplification” comment might be needed.
4. Why do we need Javadocs in public APIs? Why isn’t writing clean code sufficient in this case?

## Model 3 - Bad Comments

* Mumbling - adding a comment because you feel you should
* Redundant Comments - simply repeats the code
* Misleading Comments - gives incorrect information
* Mandated Comments - because a team or tool rule requires a comment
* Journal Comments - history of changes
* Noise Comments - provide no information
* Don’t Use a Comment When You Can Use a Function or a Variable
* Position Markers
* Closing Brace Comments
* Attributions and Bylines - who added something
* Commented-Out Code
* HTML Comments - make the comments hard to read in the IDE
* Too Much Information
* Javadocs in Nonpublic Code

```java
private void createCustomers() {
  int shortest;
  Customer c;

  while(true) {

    // Determine the shortest line.
    shortest = 0;
    for (int i = 1; i < lines.length; i++) {
      if (lines[i].size() < lines[shortest].size())
        shortest = i;
    }

    // Add the customer to the shortest time.
    c = new Customer(MAX_SERVICE_TIME);
    lines[shortest].add(c);
    System.out.println("Customer " + c.getCustomerNumber() + 
      " added to line " + shortest + ": " + lines[shortest]);
  }
}
```

### Model 3 - Questions (8 min)

1. What is a redundant comment?
2. What is a misleading comment?
3. Which of the types of bad comments listed above are no longer needed because of version control systems?
4. How should the comments in the example code in the model be dealt with?
5. What is likely the problem if you feel the need to add closing brace comments?

---

![https://xkcd.com/1421/](https://imgs.xkcd.com/comics/future_self.png)

---

Copyright © 2023 Karl R. Wurst. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
