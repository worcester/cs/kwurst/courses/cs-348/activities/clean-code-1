# Clean Code 5 - Formatting

## Content Learning Objectives

By the end of this activity, participants will be able to...

## Process Skill Goals

_During the activity, students should make progress toward:_

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: Formatting

* The Purpose of Formatting
* Team Rules

### Model 1 - Questions (5 min)

1. Why is formatting important?
2. How should you format your code when you disagree with your development team’s rules? Why?

## Model 2 - Vertical Formatting

* The Newspaper Metaphor
    > Think of a well-written newspaper article. You read it vertically. At the top you expect a headline that will tell you what the story is about and allows you to decide whether it is something you want to read. The first paragraph gives you a synopsis of the whole story, hiding all the details while giving you the broad-brush concepts. As you continue downward, the details increase until you have all the dates, names, quotes, claims, and other minutia.
* Vertical Openness Between Concepts - blank lines
* Vertical Density - tightly related code should appear vertically dense
* Vertical Distance - closely related concepts should be kept vertically close
  * Variables should be declared close to where they are used
  * Functions used by code should appear soon after that code

```java
import java.util.Scanner;

public class PrimeGenerator {

  public static void main(String args[]) {
    Scanner console = new Scanner(System.in);

    do {
      int num = getPossiblePrime(console);
      printPrimeYesNo(num);
      printPrimeFactors(num);
      printDistinctPrimeFactors(num);
    } while (repeatAgain(console));
  }

  public static int getPossiblePrime(Scanner console) {
    System.out.print("Please enter a positive integer >= 2: ");
    int num = console.nextInt();
    while (num < 2) {
      System.out.println("You did not input an integer >= 2");
      System.out.print("Please enter a positive integer >= 2: ");
      num = console.nextInt();
    }
    return num;
  }

  public static void printPrimeYesNo(int num) {    
    if (isPrime(num))
      System.out.println(num + " is a prime number");
    else
      System.out.println(num + " is not a prime number");
  }

  public static void printPrimeFactors(int num) {
    System.out.println("Prime factors of " + num + ":");
    int factor = 2;
    while (num > 1) {
      num = repeatedlyPrintAndFactorOut(num, factor);
      factor++;
    }
    System.out.println();
  }

  public static int repeatedlyPrintAndFactorOut(int num, int factor) {                                 
    while (evenlyDivisible(num, factor)) {
      System.out.print(factor + " ");
      num = num / factor;
    }
    return num;
  }

  public static void printDistinctPrimeFactors(int num) {
    System.out.println("Distinct prime factors of " + num + ":");
    int factor = 2;
    while (num > 1) {
      num = printOnceAndRepeatedlyFactorOut(num, factor);
      factor++;
    }
    System.out.println();
  }

  public static int printOnceAndRepeatedlyFactorOut(int num, int factor) {                                            
    if (evenlyDivisible(num, factor)) {
      System.out.print(factor + " ");
      while (evenlyDivisible(num, factor)) {
        num = num / factor;
      }
    }
    return num;
  }

  public static boolean isPrime(int num) {
    for (int i = 2; i <= Math.sqrt(num); i++)
      if (evenlyDivisible(num, i))
        return false;
    return true;
  }

  public static boolean evenlyDivisible(int dividend, int divisor) {
    return dividend % divisor == 0;
  }

  public static boolean repeatAgain(Scanner console) {
    System.out.print("\nTry again? Enter Yes or No: ");
    String response = console.next();
    return response.equalsIgnoreCase("yes");
  }
}
```

### Model 2 - Questions

1. Explain how the code in the model demonstrates the “newspaper metaphor”. Refer to method names.
2. Explain how the code in the model demonstrates the concept of “vertical openness”. Refer to method names.
3. Explain how the code in the model demonstrates the concept of "vertical distance" in terms of each of the following. Refer to method names.
    * Instance variables
    * Dependent functions

## Model 3 - Horizontal Formatting

* Lines should not be more than 50-100 characters long
* Horizontal Openness and Density - use whitespace to group related things, and separate unrelated things
* Indentation - helps with determining scope

### Model 3 - Questions (8 min)

1. Explain how the code in the Model 2 demonstrates “horizontal openness and density”? Refer to specific examples.

## Don't let this be your code

![http://xkcd.com/1695/](https://imgs.xkcd.com/comics/code_quality_2.png)

---

Copyright © 2023 Karl R. Wurst. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
