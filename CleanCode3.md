# Clean Code 3 - Functions

Functions are the building blocks of programs. Creating easy to read functions makes it easier to understand and modify our programs.

## Content Learning Objectives

By the end of this activity, participants will be able to...

* Explain why a variable, function, method, or class name is a bad name.
* Suggest a better name.

## Process Skill Goals

_During the activity, students should make progress toward:_

* Evaluate names. (Critical Thinking)
* Formulate good names. (Oral and Written Communication)

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: Function Size and Structure

* Functions should be small - between 2 and four lines long for the body
* Functions should do one thing. They should do it well. They should do it only.
* All statements should be at the same level of abstraction
* Blocks and Indenting (nested structures)
  * Blocks in `if`, `else`, and `while` statements should be one line long - probably a function call.
  * Functions should not have an indentation level of more than 2.
* Sections within functions is an indication that the function is doing more than one thing.
* Switch statements - always do *N* things. If absolutely necessary, it's better to bury them in a low level class and use polymorphism.
* Use Descriptive Names

```java
private void createCustomers() {
  int shortest;
  Customer c;

  while(true) {
 
    // Determine the shortest line.
    shortest = 0;
    for (int i = 1; i < lines.length; i++) {
      if (lines[i].size() < lines[shortest].size())
      shortest = i;
      }
 
    // Add the customer to the shortest time.
    c = new Customer(MAX_SERVICE_TIME);
    lines[shortest].add(c);
    System.out.println("Customer " + c.getCustomerNumber() + 
      " added to line " + shortest + ": " + lines[shortest]);
  }
}
```

```java
public static void printPrimeFactors(int num) {
  System.out.println("Prime factors of " + num + ":");
    int factor = 2;
    while (num > 1) {
      num = repeatedlyPrintAndFactorOut(num, factor);
      factor++;
    }
    System.out.println();
}
```

### Model 1 - Questions (10 min)

1. How small should functions be?
2. How many statements should typically be inside the body of an if-statement, while-loop, etc.?
3. Identify statements (or groups of statements) in the createCustomers method that have different levels of abstraction. Explain your reasoning.
4. Does the printPrimeFactors method have statements on different levels of abstraction? Why or why not?
5. What could you do to have the createCustomers method have all of its statements at the same level of abstraction?
6. How many levels of nested structures (if-statements, while-loops, etc.) should a function typically have?
7. How many "sections" should be in a function?
8. What's the problem with switch statements? What can we do about them?

## Model 2 - Function Arguments

* Number of arguments - the more arguments a function has, the harder it is to understand
  * Zero arguments is the best
  * One or two arguments are ok
  * Three arguments - avoid where possible
  * More than three arguments - don't use
* Flag (boolean) argument - means the function does more than one thing
* Argument Objects - if you need to pass multiple arguments, they are probably conceptually related, and should be wrapped in an object
* Verbs and Keywords

### Model 2 - Questions (6 min)

1. How many parameters should a function have?
2. What's wrong with functions with many parameters?
3. How can we use objects to reduce the number of parameters?
4. What's wrong with flag parameters?

## Model 3 - Function Behavior

* Have no side effects - a side effect makes changes in addition to the main purpose of the function
* Output arguments - return values, don't modify arguments
* Command query separation - functions should do something or answer something, but not both

### Model 3 - Questions (8 min)

1. Why does Uncle Bob consider side effects lies?
2. What is meant by "command query separation"?
3. Is the following method a violation of “command query separation”? Why or why not?

    ```java
        public double deposit(double amount) {
            balance = balance + amount;
            return balance;
        }
    ```

4. How would you fix this? Make sure that you can still do everything you could with the original code. If you are assuming the existence of another method, specify the method header and describe what it does.

## Model 4 - Other (5 min)

* Prefer Exceptions to Returning Error Codes - error codes force code to deal with errors immediately within the "happy path" and makes it more complicated. Exceptions let you move the error handling outside the happy path.
* Error Handling is One Thing
* Don't Repeat Yourself
* Structured Programming

### Model 4 - Questions

1. What does "happy path" mean?
2. What's the trouble with returning error codes?
3. DRY - Don't Repeat Yourself - "Duplication may be the root of all evil." Why?

## Don't let this be your code

![https://xkcd.com/1513/](https://imgs.xkcd.com/comics/code_quality.png)

---

Copyright © 2023 Karl R. Wurst. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
