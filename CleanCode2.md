# Clean Code 2 - Meaningful Names

Good names for variables, functions, methods, and classes make code easy to read. Bad names make it nearly impossible.

## Content Learning Objectives

By the end of this activity, participants will be able to...

* Explain why a variable, function, method, or class name is a bad name.
* Suggest a better name.

## Process Skill Goals

_During the activity, students should make progress toward:_

* Evaluate names. (Critical Thinking)
* Formulate good names. (Oral and Written Communication)

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: Use Intention-Revealing Names

* Names should reveal intent. If a name requires a comment, then the name does not reveal intent.

### Model 1 - Questions (5 min)

1. What should should the name of a function, variable, or class tell you about itself?
2. What does it mean if you feel the need to use a comment?
3. Which name do you think is best - `amount`, `amountInGallons`, `amountToAdd`, `amountToAddInGallons`? Explain why you think so.
4. Give your own example of a “bad” name.
5. Give your own example of a “good” name, maybe for the name you gave in question 3.

## Model 2 - Avoid Disinformation (3 min)

* Avoid Disinformation - a name should not leave false cues that obscure the meaning of the code.

### Model 2 - Questions

1. How would you find out how many students are stored in `studentList` (in Java)?
2. Would your answer be the same if you saw the following declaration? `Student[] studentList`
3. How much extra work would it add to your job to have to go look up the declaration?

## Model 3 - Make Meaningful Distinctions (3 min)

* Differences in similar names should mean something.

### Model 3 - Questions

1. Given the method with the header

    ```java
    public static void arrayCopy(int[] a1, int[] a2)
    ```

    which variable represents the original array, and which represents the copy?
2. Can you be sure? Why or why not?
3. What would be better variable names?

## Model 4 - Use Searchable Names

* Names should make it easy for find information using your IDE's search functions.
* Single-letter names are hard to search for. Use them only for local variables
 within short functions.

### Model 4 - Questions (3 min)

1. Imagine that you are writing code for a calendar program for the Commonwealth of Massachusetts. The Commonwealth has decided to change from fiscal years that start on July 1st, to starting on January 1st. You need make the month change in lots of places. What are you going to search for?
2. If you do a global search and replace, what bug are you likely to introduce into your code?
3. How would you avoid this problem?

## Model 5 - Class Names and Method Names

### Model 5 - Questions

1. Class names should be based on what part of speech? Why?
2. Method names should be based on what part of speech? Why?

## Model 6 - Add Meaningful Context/Don’t Add Gratuitous Context

The main method of a program that creates `Car` and `Truck` objects, asks the user how many gallons of fuel to add to the cars and trucks, and then adds the fuel. In this main method a variable named `gallonsOfFuelToAddToCar` might be a good name.

### Model 6 - Questions

1. If we have the following method in the Car class:

    ```java
    public void addFuel(int gallonsOfFuelToAddToCar)
    ```

    is `gallonsOfFuelToAddToCar` still a good variable name? Why or why not?
2. What would be a better variable name? Why?

## Don't let this be your code

![https://xkcd.com/1833/](https://imgs.xkcd.com/comics/code_quality_3.png)

---

Copyright © 2023 Karl R. Wurst. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
