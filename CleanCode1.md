# Clean Code 1

We will be working our way through the first 5 chapters of Clean Code by Robert C. Martin (AKA Uncle Bob). Clean Code is an opinionated book about what makes code “good” or “bad” and how to write “good” clean code.

You may or may not agree with everything the author says. And what the author suggests may or may not apply in all projects. However, personally, I have found that Uncle Bob’s book has drastically changed the way I program for the better. Even if you do not agree, I believe that by reading this book you will learn more about what works for you and what does not.

>Writing clean code is what you must do in order to call yourself a professional. There is no reasonable excuse for doing anything less than your best.
>
> Robert C. Martin, *Clean Code*

## Content Learning Objectives

By the end of this activity, participants will be able to...

* Identify general characteristics of clean code.
* Explain why clean code is important.

## Process Skill Goals

_During the activity, students should make progress toward:_

* Carefully reading source code for understanding. (Information Processing)

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: Bad Code

> The Primal Conundrum
>
> Programmers face a conundrum of basic values. All developers with more than a few years experience know that previous messes slow them down. And yet all developers feel the pressure to make messes in order to meet deadlines. In short, they don’t take the time to go fast!
>
>True professionals know that the second part of the conundrum is wrong. You will not make the deadline by making the mess. Indeed, the mess will slow you down instantly, and will force you to miss the deadline. The only way to make the deadline—the only way to go fast—is to keep the code as clean as possible at all times.
>
> Robert C. Martin, *Clean Code*
---
> Clean code is simple and direct. Clean code reads like well-written prose. Clean code never obscures the designer’s intent but rather is full of crisp abstractions and straightforward lines of control.
>
> Grady Booch, author of *Object Oriented Analysis and Design with Applications*
---
> You know you are working on clean code when each routine you read turns out to be pretty much what you expected. You can call it beautiful code when the code also makes it look like the language was made for the problem.
>
> Ward Cunningham, inventor of Wiki, inventor of Fit, coinventor of eXtreme Programming. Motive force behind Design Patterns. Smalltalk and OO thought leader. The godfather of all those who care about code.

### Model 1 - Questions

1. Why would someone write bad code?
2. Why have you written bad code in the past?
3. Read the quote from Grady Booch. What does he think is most important in clean code?
4. Read the quote from Ward Cunningham. What do you think he means by the first sentence?
5. Uncle Bob claims "the ratio of time spent reading vs. writing is well over 10:1. We are constantly reading old code as part of the effort to write new code." Why is this important?

## Model 2 - Bad Code Example

```java
import java.util.Scanner;
public class BadCode {
  public static void main(String args[]) {
    Scanner console = new Scanner(System.in);
    System.out.print("Enter a number: ");
    int num = console.nextInt();
    if (num >= 2) {
      boolean is = true;
      for (int i = 2; i <= Math.sqrt(num); i++)
        if (num % i == 0)
          is = false;
      if (is)
        System.out.println(num + " is");
      else
        System.out.println(num + " is not");
      int f = 2;
      int n = num;
      while (n > 1) {
        while (n % f == 0) {
          System.out.print(f + "  ");
          n = n / f;
        }
        f++;
      }
      System.out.println();
      f = 2;
      n = num;
      while (n > 1) {
        if (n % f == 0) {
          System.out.print(f + " ");
          while (n % f == 0) {
            n = n / f;
          }
        }
        f++;
      }
      System.out.println();
    }
  } 
}
```

### Model 2 - Questions (10 min)

1. What does this code do? (**stop after 5 minutes**)
2. What makes this code hard to understand?

## Model 3 - Clean Code Example

```java
import java.util.Scanner;

public class PrimeGenerator {

  public static void main(String args[]) {
    Scanner console = new Scanner(System.in);

    do {
      int num = getPossiblePrime(console);
      printPrimeYesNo(num);
      printPrimeFactors(num);
      printDistinctPrimeFactors(num);
    } while (repeatAgain(console));
  }

  public static int getPossiblePrime(Scanner console) {
    System.out.print("Please enter a positive integer >= 2: ");
    int num = console.nextInt();
    while (num < 2) {
      System.out.println("You did not input an integer >= 2");
      System.out.print("Please enter a positive integer >= 2: ");
      num = console.nextInt();
    }
    return num;
  }

  public static void printPrimeYesNo(int num) {    
    if (isPrime(num))
      System.out.println(num + " is a prime number");
    else
      System.out.println(num + " is not a prime number");
  }

  public static void printPrimeFactors(int num) {
    System.out.println("Prime factors of " + num + ":");
    int factor = 2;
    while (num > 1) {
      num = repeatedlyPrintAndFactorOut(num, factor);
      factor++;
    }
    System.out.println();
  }

  public static int repeatedlyPrintAndFactorOut(int num, int factor) {
    while (evenlyDivisible(num, factor)) {
      System.out.print(factor + " ");
      num = num / factor;
    }
    return num;
  }

  public static void printDistinctPrimeFactors(int num) {
    System.out.println("Distinct prime factors of " + num + ":");
    int factor = 2;
    while (num > 1) {
      num = printOnceAndRepeatedlyFactorOut(num, factor);
      factor++;
    }
    System.out.println();
  }

  public static int printOnceAndRepeatedlyFactorOut(int num, int factor) {
    if (evenlyDivisible(num, factor)) {
      System.out.print(factor + " ");
      while (evenlyDivisible(num, factor)) {
        num = num / factor;
      }
    }
    return num;
  }

  public static boolean isPrime(int num) {
    for (int i = 2; i <= Math.sqrt(num); i++)
      if (evenlyDivisible(num, i))
        return false;
    return true;
  }

  public static boolean evenlyDivisible(int dividend, int divisor) {
    return dividend % divisor == 0;
  }

  public static boolean repeatAgain(Scanner console) {
    System.out.print("\nTry again? Enter Yes or No: ");
    String response = console.next();
    return response.equalsIgnoreCase("yes");
  }
}

```

### Model 3 - Questions (15 min)

1. What does this code do? (*stop after 5 minutes*)
2. Do you feel that you understand this code better than you did the code in Model 4?
3. At what point did you understand enough of what the overall program does?
4. If you needed more detail on how it works, how hard is it to find what you need?
5. List at least 3 things that made this code easier to read.
6. Were you surprised that you did not need comments to help you understand the code?

---

![https://www.osnews.com/images/comics/wtfm.jpg](https://www.osnews.com/images/comics/wtfm.jpg)

---

Copyright © 2023 Karl R. Wurst. This work is licensed under a  Creative  Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
